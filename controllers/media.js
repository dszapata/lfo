(() => {
  var app = angular.module('team');

  app.controller('mediaController', function($scope, $http) {

    $scope.gruposVideos = [];

    function cargarVideos(){
      // return $http.get('http://teamlfo.webcindario.com/data/videos.txt');
      return $http.get('file:.//data/videos.txt');
    }

    $scope.getVideo = function(url){
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
      var match = url.match(regExp);

      if (match && match[2].length == 11) {
          return "https://www.youtube.com/embed/" + match[2];
      } else {
          return 'error';
      }
    }

    $scope.sortVideo = function(video) {
      var date = new Date(video.fecha);
      return date;
    };

    $scope.scroll = function(direccion, grupo) {
      var scroll_container = document.getElementById('global'+grupo);
      if (direccion === 1){
        scroll_container.scrollLeft -= 1000;
      }else if (direccion === 2){
        scroll_container.scrollLeft += 1000;
      }
    }

    function inicio(){
      cargarVideos().then(
        (resultado =>{
            console.log(resultado.data);
            $scope.gruposVideos = resultado.data;
            $scope.gruposVideos.forEach((grupo) =>{
              console.log(grupo.grupo);
            })
        })
      )
    }

    inicio();
  })
})();
