(() => {
  var app = angular.module('team');
  app.filter('trustAsResourceUrl', ['$sce', function($sce) {
      return function(val) {
          return $sce.trustAsResourceUrl(val);
      };
  }]);
  app.controller('teamController', function($scope, $http) {
    $scope.sortType = "";
    $scope.sortReverse = "";
    $scope.roadSelected = false;
    $scope.ovalSelected = false;
    $scope.loading = false;
    $scope.gruposVideos = [];

    let CLASE = 'Class';
    let INCIDENTES = 'AvgInc';

    var pilotos=[];
    pilotos[232222] = {
          id: 232222,
          youtube: 'https://www.youtube.com/user/danijg110/featured',
          twitter: 'https://twitter.com/dani_simgp',
          name: 'Daniel Jim\u00E9nez'
        };
    pilotos[261697] = {
      id: 261697,
      youtube: 'https://www.youtube.com/channel/UCDZsfo1T08GuQvlsKygXVjA/featured',
      twitter: 'https://twitter.com/Miromen83',
      name: 'Marc Mir\u00F3'
    };
    pilotos[266243] = {
      id: 266243,
      youtube: 'https://www.youtube.com/user/joancerve/featured',
      twitter: 'https://twitter.com/espidi89',
      name: 'Joan Espi'
    };
    pilotos[264425] = {
      id: 264425,
      name: 'Xabi Mulas'
    };
    pilotos[233927] = {
      id: 233927,
      youtube: 'https://www.youtube.com/channel/UCm6S2BhkntpqPzQbFwI-5_g/featured',
      twitter: 'https://twitter.com/JoseAntonioFR90',
      name: 'Jose Antonio Fern\u00E1ndez Rubio'
    };
    pilotos[75490] = {
      id: 75490,
      youtube: 'https://www.youtube.com/user/frantoledano/featured',
      twitter: 'https://twitter.com/Fran_Toledano',
      name: 'Fran Toledano'
    };
    pilotos[256286] = {
      id: 256286,
      name: 'Eduardo Salido Pi\u00F1eiro'
    };
    pilotos[310141] = {
      id: 310141,
      name: 'Cristian Josue Streitemberger'
    };
    pilotos[299144] = {
      id: 299144,
      youtube: 'https://www.youtube.com/channel/UChJYJO_wIUBAwrwaP6iPOng/featured',
      twitter: 'https://twitter.com/Agentspecial008',
      name: 'Miguel Castro'
    };
    pilotos[256078] = {
      id: 256078,
      youtube: 'https://www.youtube.com/channel/UC7ur2wzHsrbsQkpUcrG366A',
      twitter: 'https://twitter.com/Francis_iracing',
      name: 'Francis Fern\u00E1ndez Rubio'
    };
    pilotos[272742] = {
      id: 272742,
      twitter: 'https://twitter.com/ric96tiesto',
      name: 'Ricardo cuando pagues te pongo apellidos'
    };
    pilotos[190617] = {
      id: 190617,
      twitter: 'https://twitter.com/mpocho1977',
      name: 'Miguel Pocho'
    };
    pilotos[290981] = {
      id: 290981,
      name: 'Armando Luque Avenger'
    };
    pilotos[279176] = {
      id: 279176,
      name: 'David Monasterio Elemento'
    };
    pilotos[280408] = {
      id: 280408,
      youtube: 'https://www.youtube.com/user/dszapata/featured',
      twitter: 'https://twitter.com/davidszapatalfo',
      name: 'David S. Zapata'
    };
    pilotos[245970] = {
      id: 245970,
      name: 'Jose Ortega'
    }
    pilotos[315585] = {
      id: 315585,
      name: 'Dani Lozano'
    }


    function cargarStats(file){
      $scope.loading = true;
      $scope.teamLFO = [];
      var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, true);
        rawFile.onreadystatechange = function ()
        {
          if(rawFile.readyState === 4)
          {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
              var allText = rawFile.responseText;
              let json = csvJSON(allText);
              json.forEach((driver)=>{
                let piloto = pilotos[parseInt(driver.CustId)];
                if (piloto){
                  driver['twitter'] = piloto.twitter;
                  driver['youtube'] = piloto.youtube;
                  driver['name'] = piloto.name;
                  $scope.teamLFO.push(driver);
                }
              })
              console.log($scope.teamLFO);
              $scope.loading = false;
              $scope.$apply();
            }
          }
        }
        rawFile.send();
    }

    function csvJSON(csv){
      csv = csv.replace(new RegExp('"', 'g'), '');
      var lines=csv.split("\n");
      var result = [];
      var headers=lines[0].replace(new RegExp(' ', 'g'), '').split(",");
      for(var i=1;i<lines.length;i++){
    	  var obj = {};
    	  var currentline=lines[i].split(",");
        if (currentline[0]){
      	  for(var j=0;j<headers.length;j++){
            let header = headers[j];
            if (j === 20){
              header = "CustId"
            }
      		  obj[header] = currentline[j];
      	  }
          result.push(obj);
        }
      }
      return result; //JavaScript object
      //return JSON.stringify(result); //JSON
    }

    function getDriverById(id, data) {
      return data.filter(
          function(data){ return data.CUSTID == id }
      );
    }

    $scope.sorterFunc = function(person){
      if (person[$scope.sortType]){
        let field = person[$scope.sortType].replace('.', '');
        if (isNaN(field)){
          if ($scope.sortType === CLASE){
            let sortLicense = sorterLicense(field.substring(0, 1));
            let sr = field.substring(2, field.length);
            sr = parseInt(sr);
            if (sr < 99){
              sr = sr + 100;
            }
            return sortLicense*1000 + sr
          }
          return field;
        }else{
          let num = parseInt(field);
          if ($scope.sortType === INCIDENTES && num < 9){
            num = parseInt(num + "00");
          }else if ($scope.sortType === INCIDENTES && num < 99){
            num = parseInt(num + "0");
          }
          return num;
        }
      }
    };

    function sorterLicense(clase){
      let license = 0;
      if (clase.indexOf("A")> -1){
        license = 5;
      }else if (clase.indexOf("B")> -1){
        license = 4;
      }else if (clase.indexOf("C")> -1){
        license = 3;
      }else if (clase.indexOf("D")> -1){
        license = 2;
      }else if (clase.indexOf("R")> -1){
        license = 1;
      }
      return license;
    }

    $scope.loadOval = function() {
      statsSelected(false, true);
      cargarStats('file:.//data/driverstatsOval.csv');
      // cargarStats('http://teamlfo.webcindario.com/data/driverstatsOval.txt')
    }

    $scope.loadRoad = function() {
      statsSelected(true, false);
      cargarStats('file:.//data/driverstatsRoad.csv');
      // cargarStats('http://teamlfo.webcindario.com/data/driverstatsRoad.txt')
    }

    $scope.scroll = function(direccion, grupo) {
      var scroll_container = document.getElementById('global'+grupo);
      if (direccion === 1){
        scroll_container.scrollLeft -= 1000;
      }else if (direccion === 2){
        scroll_container.scrollLeft += 1000;
      }
    }

    function statsSelected(road, oval){
      $scope.roadSelected = road;
      $scope.ovalSelected = oval;
    }

    function cargarVideos(){
      // return $http.get('http://teamlfo.webcindario.com/data/videos.txt');
      return $http.get('file:.//data/videos.txt');
    }

    $scope.getVideo = function(url){
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
      var match = url.match(regExp);

      if (match && match[2].length == 11) {
          return "https://www.youtube.com/embed/" + match[2];
      } else {
          return 'error';
      }
    }

    $scope.sortVideo = function(video) {
      var date = new Date(video.fecha);
      return date;
    };

    // $scope.resize(){
    //
    // }


    function inicio(){
      cargarVideos().then(
        (resultado =>{
            console.log(resultado.data);
            $scope.gruposVideos = resultado.data;
            $scope.gruposVideos.forEach((grupo) =>{
              console.log(grupo.grupo);
            })
            $scope.loadRoad();
        })
      )
    }

    inicio();

  })
})();
