var myApp = angular.module('team', ['ui.router']);

myApp.config(function($stateProvider, $urlRouterProvider) {
  var helloState = {
    name: 'home',
    url: '/home',
    templateUrl: './pages/home.html'
  }

  var aboutState = {
    name: 'team',
    url: '/team',
    templateUrl: './pages/team.html'
  }

  var mediaState = {
    name: 'media',
    url: '/media',
    templateUrl: './pages/media.html'
  }

  var calendarState = {
    name: 'calendar',
    url: '/calendar',
    templateUrl: './pages/calendar.html'
  }

  $stateProvider.state(helloState);
  $stateProvider.state(aboutState);
  $stateProvider.state(mediaState);
  $stateProvider.state(calendarState);

  $urlRouterProvider.otherwise('home');
});
